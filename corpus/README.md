[comment]: <> "LTeX: language=fr"
<!-- markdownlint-disable MD025 -->

Corpus
======

Organisation :

- Les arbres dont les annotations ont été complètement manuellement vérifiées sont dans
  [`gold/`](gold/).
  - Les fichiers dans [`gold/conllu/`](gold/conllu/) avec la segmentation en phrases, les POS, les
    têtes et types de dépendances et les lemmes s'ils sont présents.
  - Les fichiers dans [`gold/tsv/`](gold/tsv/) avec les lemmes et POS.
- Dans [`ore/`](ore/), les fichiers CoNLL-U correspondant aux blocs pour lesquels il n'y a pas de
  gold, segmentés automatiquement et sans annotations (ou si annotations il y a elles ne sont pas à
  prendre en compte). 
- Dans [`silver/`](silver), un sous-dossier par parser, qui contient les mêmes fichiers CoNLL-U que
  `ore/`, mais avec les annotations prédites par le parser en question.

Aucun de ces dossiers ne contient d'autres fichiers ou dossiers.

**Important** : sur la branche principale (`main`), tous ces fichiers doivent être synchronisés, c'est-à-dire que :

- Pour les arbres qui sont présents à la fois dans  [`gold/conllu/`](gold/conllu/) et
  [`gold/tsv/`](gold/tsv/), les POS et lemmes doivent être identiques.
- Les arbres dans [`ore/`](ore/) et dans [`silver/`](silver/) doivent être exactement les mêmes,
  dans des fichiers avec exactement les mêmes noms et dans le même ordre. Les seules différences
  permises sont les annotations prédites.
- Il ne doit pas y avoir d'arbres qui soient à la fois dans [`gold/`](gold/) et dans [`ore/`](ore/) (donc pas non plus dans [`silver/`](silver/))

Dans la mesure du possible, ces garanties sont validées automatiquement à chaque commit et doivent
être vérifiées pour un commit sur la branche `main`. Pour faire un commit qui ne vérifie pas ces
conditions il faut être sur une autre branche. Par exemple pour ajouter de nouvelles annotations manuelles il faut :

1. Faire un commit sur une nouvelle branche de travail
2. Le signaler aux personnes responsables des autres parties et leur demander les modifications
   adéquates (par exemple les maintainers de chaque parser doivent refaire un cycle d'entraînement
   et annotation). Ces modifications sont à pousser sur la branche de travail.
3. Pour intégrer les modifications à `main`, faire un *merge request* sur Gitlab.